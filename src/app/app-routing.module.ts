import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardApiComponent } from './dashboard-api/dashboard-api.component';
import {FourOfFourComponent} from "./four-of-four/four-of-four.component";
import {SignupComponent} from "./signup/signup.component";
import {RestaurantViewComponent} from "./restaurant-view/restaurant-view.component";
import {EditRestaurantComponent} from "./edit-restaurant/edit-restaurant.component";
import {EditUserComponent} from "./edit-user/edit-user.component";
import {AdminAuthGuardService} from "./services/admin-auth-guard.service";
import {LiveryAuthGuardService} from "./services/livery-auth-guard.service";
import {DelivererComponent} from "./deliverer/deliverer.component";
import {RestaurantAuthGuardService} from "./services/restaurant-auth-guard.service";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'api', canActivate: [AdminAuthGuardService],  component: DashboardApiComponent},
  { path: 'restaurants', canActivate: [RestaurantAuthGuardService], component: RestaurantViewComponent},
  { path: 'restaurants/:id', canActivate: [RestaurantAuthGuardService], component: EditRestaurantComponent},
  { path: 'livreur', canActivate: [LiveryAuthGuardService], component: DelivererComponent},
  { path: 'signupr', canActivate: [AdminAuthGuardService], component: SignupComponent},
  { path: 'signupr/:id', canActivate: [AdminAuthGuardService], component: EditUserComponent},
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: '**', component: FourOfFourComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

