import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ModalHourComponent } from '../modal-hour/modal-hour.component';
import {ControllerService} from "../services/controller.service";


@Component({
  selector: 'app-dashboard-api',
  templateUrl: './dashboard-api.component.html',
  styleUrls: ['./dashboard-api.component.css']
})
export class DashboardApiComponent {

  posts$: Observable<any[]>;

  constructor(private wp: ControllerService, public dialog: MatDialog) {
    this.posts$ = this.wp.getPosts();
  }

clicked = false;
clicked_2 = true;

accepter(){
  console.log('Commande acceptée');
  this.clicked = true;
  this.dialog.open(ModalHourComponent);
  // const dialogRef = this.dialog.open(ModalHour);
  // dialogRef.afterClosed().subscribe(result => {
  //   console.log(`Dialog result: ${result}`);
  // });

}




finir(){
    console.log('Commande finie');
    if (this.clicked == true){
      this.clicked_2 = false;
    }
    this.clicked = true;
}

}

export class ModalHour {}

