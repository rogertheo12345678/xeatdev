import { Component, OnInit } from '@angular/core';
import {ControllerService} from "../services/controller.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms"
import swal from 'sweetalert2';


export class Restaurant {
  constructor(
  ) {}
}

@Component({
  selector: 'app-edit-restaurant',
  templateUrl: './edit-restaurant.component.html',
  styleUrls: ['./edit-restaurant.component.css']
})
export class EditRestaurantComponent implements OnInit {

  editForm: FormGroup;
  public id: string;
  public data: any;


  constructor(private wp: ControllerService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getByID(id);
    this.editForm = this.formBuilder.group({
      id_restaurant: ['', Validators.required],
      restaurant_name: ['', Validators.required]
    });

  }

  getByID(id: string)
  {
    this.wp.getOneById(id).subscribe(res => {
      this.data = res;
    });
  }
   updateRestaurant(id: string){
    this.wp.putRestaurant(id, this.editForm.value).subscribe(()=>{
      // console.log('Restaurant update');
      // console.warn(this.editForm.value);
        swal.fire({
          title:'Modification',
          text:'Le restaurant à été correctement modifié',
          icon:'success',
          timer: 2000,
          toast: true,
          timerProgressBar:true,
          showConfirmButton:false,
                    })
      this.router.navigate(['restaurants'])
    },
      err => swal.fire({
        title:'Erreur',
        text:'La modification n\'a pas marché',
        icon:'warning',
        timer: 2000,
        toast: true,
        timerProgressBar:true,
        showConfirmButton:false,
      }),
    )
  }




}
