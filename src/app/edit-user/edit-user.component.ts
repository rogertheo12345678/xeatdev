import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ControllerService} from "../services/controller.service";
import {ActivatedRoute, Router} from "@angular/router";
import swal from "sweetalert2";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  editForm: FormGroup;
  public id: string;
  public data: any;


  constructor(private wp: ControllerService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getByID(id);
    console.log(id)

    this.editForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email_address: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      restaurant_id:['1', Validators.required],
      role:['1', Validators.required]
    });
  }

  getByID(id: string)
  {
    this.wp.getUserById(id).subscribe(res => {
      this.data = res;
    });
  }


  updateUser(id: string){
    this.wp.putUser(id, this.editForm.value).subscribe(()=>{
      console.log('Restaurant update');
      swal.fire({
        title:'Modification',
        text:'L\'utilisateur à été correctement modifié',
        icon:'success',
        timer: 2000,
        toast: true,
        position:"center",
        timerProgressBar:true,
        showConfirmButton:false,
      })
      this.router.navigate(['signupr'])
    })
  }




}
