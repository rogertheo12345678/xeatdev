import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {LoginService} from "../services/login.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginform: FormGroup;
  errorMessage: string;
  hide = true;

  constructor(private formbuilder: FormBuilder,
              private router: Router,
              public authService: AuthService,
              public loginService: LoginService) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.loginform = this.formbuilder.group({
      email_address: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onLoginForm() {
    this.loginService.loginPatient(Object.assign({}, this.loginform.value)).subscribe((value:any) => {
      this.authService.signIn(value.token);
      this.authService.getRoleUserToken(value.token)
      this.router.navigate(['/restaurants']);
    },
      error => {
      this.errorMessage = 'Votre mot de passe ou votre email est incorrect. Réessayez !'
      }
    )

    };
}

