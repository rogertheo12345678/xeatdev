import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ControllerService {

  urlApi = "http://localhost:3000/"
  constructor(private http: HttpClient) { }

  //Commande
  getPosts(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:3000/api/woo');
  }

  //Restaurant

  getRestaurants(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:3000/api/restaurants');
  }

  getOneById(id:string): Observable<any[]>{
    return this.http.get<any[]>('http://localhost:3000/api/restaurants/'+ id)
  }

  deleteRestaurant(postId:string){
    this.http.delete('http://localhost:3000/api/restaurants/'+postId)
      .subscribe(()=>{
        console.log("Deleted");
      });
  }
  putRestaurant(id: string, params: any) {
    return this.http.put('http://localhost:3000/api/restaurants/'+id, params);
  }

  //Users

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:3000/api/users');
  }

  getUserById(id:string): Observable<any[]>{
    return this.http.get<any[]>('http://localhost:3000/api/users/'+ id)
  }

  deleteUser(id:string){
    this.http.delete('http://localhost:3000/api/users/'+id)
      .subscribe(()=>{
        console.log("Deleted");
      });
  }
  putUser(id: string, params: any) {
    return this.http.put('http://localhost:3000/api/users/'+id, params);
  }

}

