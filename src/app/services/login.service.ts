import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public httpClient: HttpClient) {
  }

  loginPatient(data: any) {
    return this.httpClient.post(`http://localhost:3000/api/auth/login`, data);
  }

}
