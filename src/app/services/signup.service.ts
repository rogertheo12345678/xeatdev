import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(public httpClient: HttpClient) { }


  signupAccount(data: any) {
    return this.httpClient.post(`http://localhost:3000/api/users`, data);
  }

  addRestaurant(data: any) {
    return this.httpClient.post(`http://localhost:3000/api/restaurants`, data);
  }

}
