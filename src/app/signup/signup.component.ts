import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SignupService} from "../services/signup.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {ControllerService} from "../services/controller.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  users$: Observable<any[]>;

  constructor(private formBuilder:FormBuilder,
              private signupService: SignupService,
              private router: Router,
              private wp:ControllerService) {
    this.users$ = this.wp.getUsers();
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.signupForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email_address: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      restaurant_id:['1', Validators.required],
      role:['1', Validators.required]
    });
  }
  onSubmitForm() {
    this.signupService.signupAccount(Object.assign({}, this.signupForm.value)).subscribe((user) => {
      this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
        this.router.navigate(['signupr']);
      });
    });
    // this.router.navigate(['/login'])

  }

  onDelete(id: string){
    this.wp.deleteUser(id);
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['signupr']);
    });
  }


}
